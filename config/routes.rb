Rails.application.routes.draw do
  get '/national_news_search', to: "national#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
